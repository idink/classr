findBestType <- function(
    x,
    acceptance = 0.95,
    priority  = c('numeric', 'datetime'),
    date.formats = c('%Y-%m-%d', '%d/%m/%Y'),
    datetime.formats = c('%Y-%m-%d %H:%M:%S'),
    time.zone = ''
) {
    for(each.type in priority) {
        if(each.type == 'numeric') {
            try.type <- tryNumeric(x = x, acceptance = acceptance)
            if(try.type$type != 'character') {
                return(try.type)
            }
        } else if(each.type == 'datetime') {
            try.type <- tryDateTime(
                x = x, acceptance = acceptance,
                date.formats = date.formats,
                datetime.formats = datetime.formats,
                time.zone = time.zone
            )
            if(try.type$type != 'character') {
                return(try.type)
            }
        }
    }

    # if unsuccessful
    try.type <- list(type = 'character', data = as.character(x))

    return(try.type)
}


# x <- c(1:10)
# findBestType(x)
#
#
# x <- c(1:10)+0.1
# findBestType(x)
#
# x <- c('2014-01-02', '2014-01-02 22:22:22')
# findBestType(x)
# tryDateTime(x, acceptance = 1/2)
